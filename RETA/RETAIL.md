# tablas

> sql
```
>use RETAIL
SELECT * FROM co.centroCostoVoucher
SELECT * FROM co.detalleVoucher
SELECT * FROM co.libros
SELECT * FROM co.periodoContable
SELECT * FROM co.planCuentas
SELECT * FROM co.voucher
SELECT * FROM co.voucherLetra
SELECT * FROM ma.almacen
SELECT * FROM ma.banco
SELECT * FROM ma.camion
SELECT * FROM ma.canal
SELECT * FROM ma.centroCosto
SELECT * FROM ma.componente
SELECT * FROM ma.condicion
SELECT * FROM ma.condicionPersona
SELECT * FROM ma.cuentaBanco
SELECT * FROM ma.departamento
SELECT * FROM ma.distrito
SELECT * FROM ma.formaPago
SELECT * FROM ma.item
SELECT * FROM ma.itemClases
SELECT * FROM ma.itemLineas
SELECT * FROM ma.itemModelo
SELECT * FROM ma.lineas
SELECT * FROM ma.modelo
SELECT * FROM ma.motivo
SELECT * FROM ma.persona
SELECT * FROM ma.personaDestino
SELECT * FROM ma.provincia
SELECT * FROM ma.receta
SELECT * FROM ma.relacionPersona
SELECT * FROM ma.saldosIniciales
SELECT * FROM ma.tipoCondicion
SELECT * FROM ma.tipoDocumento
SELECT * FROM ma.tipoPersona
SELECT * FROM ma.vendedor
SELECT * FROM ma.zonas
SELECT * FROM ma.zonasPersona
SELECT * FROM pa.pago
SELECT * FROM pa.pagoVoucher
SELECT * FROM plani.afp
SELECT * FROM plani.afpConcepto
SELECT * FROM plani.cabeceraPlanilla
SELECT * FROM plani.Cargos
SELECT * FROM plani.categoria
SELECT * FROM plani.concepto
SELECT * FROM plani.detalleConcepto
SELECT * FROM plani.detallePlanilla
SELECT * FROM plani.empleado
SELECT * FROM plani.Etiqueta
SELECT * FROM plani.etiquetaConcepto
SELECT * FROM plani.evaluacion
SELECT * FROM plani.gradoInstruccion
SELECT * FROM plani.objetivo
SELECT * FROM plani.periodo
SELECT * FROM plani.planilla
SELECT * FROM plani.puntuacion
SELECT * FROM plani.rendimiento
SELECT * FROM plani.tipoConcepto
SELECT * FROM plani.tipoDocumento
SELECT * FROM plani.vias
SELECT * FROM ve.documento
SELECT * FROM ve.documentoCondicion
SELECT * FROM ve.documentoDestino
SELECT * FROM ve.documentoDetalle
SELECT * FROM ve.documentoPago
SELECT * FROM ve.documentosCanjeados
SELECT * FROM ve.documentoTransportista
SELECT * FROM ve.documentoVoucher
SELECT * FROM ve.entregables
SELECT * FROM ve.generacionDocumentos
SELECT * FROM ve.generacionLetras
```
## EJERCICIO 1


```
--DECLARE @fecha_inicio datetime = '2009-01-01';
--DECLARE @fecha_fin datetime = '2009-12-31';

--DECLARE @fecha_inicio datetime = '2008-01-01';
--DECLARE @fecha_fin datetime = '2008-12-31';

--DECLARE @fecha_inicio datetime = '2007-01-01';
--DECLARE @fecha_fin datetime = '2007-12-31';

--DECLARE @fecha_inicio datetime = '2006-01-01';
--DECLARE @fecha_fin datetime = '2006-12-31';

DECLARE @fecha_inicio datetime = '2005-01-01';
DECLARE @fecha_fin datetime = '2005-12-31';
```
```
SELECT SUM(total), FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
WHERE fechaMovimiento BETWEEN @fecha_inicio AND @fecha_fin;
```
## EJERCICIO 2

```
SELECT q.* FROM ma.persona q
LEFT JOIN ma.personaDestino ki ON q.persona = ki.persona
WHERE ki.persona IS NULL;
```
## EJERCICIO 3

```
SELECT FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento;
SELECT * FROM ve.documento
WHERE total > (SELECT AVG(total) FROM ve.documento);

```
## EJERCICIO 5
```
SELECT d.* FROM ve.documentoPago dp
INNER JOIN ve.documento d ON dp.documento = d.documento
INNER JOIN pa.pago p ON dp.pago = p.pago
WHERE p.formaPago = 10;
```
## EJERCICIO 6

```
SELECT p.*, r.* FROM ve.documento p
INNER JOIN ve.documentosCanjeados r ON p.documento = r.documento01 OR p.documento = r.documento02
```
```
SELECT p.*, r.* FROM ve.documento p
INNER JOIN ve.documentosCanjeados r ON p.documento = r.documento01 AND p.documento = r.documento02
```
## EJERCICIO 7

```
SELECT almacen, SUM(costoSoles) AS Saldo_Total
FROM ma.saldosIniciales
GROUP BY almacen
```
## EJERCICIO 8 

```
SELECT d.* FROM ve.documento d
INNER JOIN ve.documentoDetalle dd ON d.documento = dd.documento
WHERE d.vendedor = 3;
```
## EJERCICIO 9

```
select * from ve.documento
SELECT vendedor, YEAR(fechaMovimiento) AS Anio, FORMAT(AVG(total), 'C', 'es-PE') AS Total_Ventas, FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
GROUP BY vendedor, YEAR(fechaMovimiento)
HAVING SUM(total) > 100000
ORDER BY (Total_ventas);
```
## EJERCICIO 10

```
SELECT vendedor, MONTH(fechaMovimiento) AS Anio, FORMAT(AVG(total), 'C', 'es-PE') AS Total_Ventas , FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
GROUP BY vendedor, MONTH(fechaMovimiento)
HAVING SUM(total) > 100000
ORDER BY (Total_ventas) DESC;
```
## EJERCICIO 11

```
SELECT persona, YEAR(fechaMovimiento) AS Año, COUNT(*) AS Compras, FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
WHERE tipoMovimiento = 1
GROUP BY persona, YEAR(fechaMovimiento)
HAVING COUNT(*) > 10
ORDER BY (Año) DESC;
```
## EJERCICIO 12
```
SELECT vendedor, FORMAT(AVG(descto01 + descto02 + descto03), 'C', 'es-PE') AS Descuentos_Acumulados, FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM ve.documento
GROUP BY vendedor
HAVING SUM(descto01 + descto02 + descto03) > 5000
ORDER BY (vendedor) DESC;
```
## EJERCICIO 13
```
SELECT persona, YEAR(fechaMovimiento) AS Año, FORMAT(AVG(total), 'C', 'es-PE') AS Total_Anual
FROM ve.documento
WHERE tipoMovimiento = 1
GROUP BY persona, YEAR(fechaMovimiento)
HAVING SUM(total) > 10000
ORDER BY Año ; 
```
## EJERCICIO 14
```
SELECT d.vendedor, 
COUNT(dd.documentoDetalle) AS Total_Productos_Vendidos
FROM ve.documentoDetalle dd
JOIN ve.documento d ON dd.documento = d.documento
GROUP BY d.vendedor;
```
## EJERCICO 15
```
SELECT MONTH(d.fechaMovimiento) AS Mes, p.formaPago, SUM(d.total) AS Total_Ventas
FROM ve.documento d
JOIN pa.pago p ON d.vendedor = p.vendedor
WHERE YEAR(d.fechaMovimiento) = 2009
GROUP BY MONTH(d.fechaMovimiento), p.formaPago;
```